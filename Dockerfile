FROM solidbeans/jdk8

COPY spring-boot-app.jar /
EXPOSE 8080
CMD $JAVA_HOME/bin/java -jar spring-boot-app.jar

